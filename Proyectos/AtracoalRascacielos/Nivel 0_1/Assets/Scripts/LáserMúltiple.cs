﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LáserMúltiple : MonoBehaviour
{


    // Declaramos una variable pública para guardar el material de la luz del láser y asi configurar el color de la luz(material rojo en este caso)
    public Material Material;
    //Una referencia a los agentes de este ejemplo para poder cambiar los valores de sus variables. Este ejemplo permite un número máximo de 5 agentes, pero se puede aumentar
    // el número con la consecuente carga en el procesado del juego
    GameObject Agente1, Agente2, Agente3, Agente4, Agente5;
    //Referencia al jugador, para decirle al agente donde está el jugador en caso de que sea detectado por un láser
    public GameObject Player;
    //Vector donde guardamos la posición a la que iba el agente antes de que el laser detectara al jugador
    Vector3 LastWP1, LastWP2, LastWP3, LastWP4, LastWP5;
    //Esta variable guarda la posición actual, en número,(cuando el laser toca al jugador) del vector de trayectorias del agente.
    int PathWP1, PathWP2, PathWP3, PathWP4, PathWP5;
    //Esta variable se usa para realizar una única vez la asignación de la posicion del jugador cuando el laser toca el jugador, si no se pone esta restricción se bugea el script
    int UnaVez = 0;
    //Esta variable se usa para definir el tiempo que los agentes buscan al jugador cuando el laser lo detecta
    public float SearchTime = 10;
    //Esta variable se usa, para poder ir recorriendo todos los agentes e ir guardando sus variables originales.
    int CantidadAgentes = 0;
    //El siguiente array se usa para poder llamar a la funcion FindGameObjectsWithtag y contar por codigo cuantos agentes hay en la partida
    GameObject[] Agents;

    void Start()
    {
        //Declaramos una variable donde almacenar el componente LineRenderer (debe haber un componente LineRenderer en el objeto de este código. Creo que 
        //para poner este componente Unity>Componentes>Efectos>LineRender)
        LineRenderer lineRenderer = GetComponent<LineRenderer>();
        //Asignamos el material creado anteriormente al material del láser
        lineRenderer.material = Material;
        //Buscamos todos los GmaeObjects con el tag Agente
        Agents = GameObject.FindGameObjectsWithTag("Agente");
        //Para cada GameObject encontrado con el Tag agente
        foreach (GameObject Agent in Agents)
        {
            //Cantidad de agente nos dice que agente estamos tocando en el momento actual, en la siguiente iteración tocaremos el siguiente agente.
            //para cada agente asignamos una variable tipo GameObject para tenerlo referenciado
            CantidadAgentes += 1;
            switch (CantidadAgentes)
            {
                case 5:
                    Agente5 = Agent;

                    break;
                case 4:
                    Agente4 = Agent;

                    break;
                case 3:
                    Agente3 = Agent;

                    break;
                case 2:
                    Agente2 = Agent;

                    break;
                case 1:
                    Agente1 = Agent;

                    break;
                default:
                    print("Incorrect intelligence level.");
                    break;
            }
        }
    }


    void Update()
    {
        //Hay que volver a asignar la variable lineRenderer al componente LineRenderer. El lineRender permite dibujar lineas en Unity.
        LineRenderer lineRenderer = GetComponent<LineRenderer>();

        //Definimos el punto de partida del laser, que sera igual a la posicion del láser.Como este código va en el laser transform.position
        lineRenderer.SetPosition(0, transform.position);

        //Lanzamos un Raycast desde el laser hacia delante(en su propio sistema de coordenadas) y almacenamos los resultados en la variable hit.
        RaycastHit hit;
        Physics.Raycast(transform.position, transform.forward, out hit);

        //Define el punto de llegada del láser, igual al primer punto de choque del raycast. Esto dibuja una linea desde el laser hasta ese punto. 
        lineRenderer.SetPosition(1, hit.point);

        //Si el láser te detecta
        if (hit.collider.tag == "Player")
        {
            //realiza esta acción una sola vez
            if (UnaVez == 0)
            {
                //si existe este agente
                if (Agente5 != null)
                {
                    //guarda la posición actual, en número,(cuando el laser toca al jugador) del vector de trayectorias del agente.
                    PathWP5 = Agente5.GetComponent<MovimientoAgenteRecuperado>().currentWP;
                    //Guarda en la variable LastWP la posición a la que iba el agente en el momento de la detección
                    LastWP5 = Agente5.GetComponent<MovimientoAgenteRecuperado>().waypointsR[PathWP5].transform.position;
                }
                if (Agente4 != null)
                {
                    //guarda la posición actual, en número,(cuando el laser toca al jugador) del vector de trayectorias del agente.
                    PathWP4 = Agente4.GetComponent<MovimientoAgenteRecuperado>().currentWP;
                    //Guarda en la variable LastWP la posición a la que iba el agente en el momento de la detección
                    LastWP4 = Agente4.GetComponent<MovimientoAgenteRecuperado>().waypointsR[PathWP4].transform.position;
                }
                if (Agente3 != null)
                {
                    //guarda la posición actual, en número,(cuando el laser toca al jugador) del vector de trayectorias del agente.
                    PathWP3 = Agente3.GetComponent<MovimientoAgenteRecuperado>().currentWP;
                    //Guarda en la variable LastWP la posición a la que iba el agente en el momento de la detección
                    LastWP3 = Agente3.GetComponent<MovimientoAgenteRecuperado>().waypointsR[PathWP3].transform.position; print("Whadya want?");
                }
                if (Agente2 != null)
                {
                    //guarda la posición actual, en número,(cuando el laser toca al jugador) del vector de trayectorias del agente.
                    PathWP2 = Agente2.GetComponent<MovimientoAgenteRecuperado>().currentWP;
                    //Guarda en la variable LastWP la posición a la que iba el agente en el momento de la detección
                    LastWP2 = Agente2.GetComponent<MovimientoAgenteRecuperado>().waypointsR[PathWP2].transform.position;
                }
                if (Agente1 != null)
                {
                    //guarda la posición actual, en número,(cuando el laser toca al jugador) del vector de trayectorias del agente.
                    PathWP1 = Agente1.GetComponent<MovimientoAgenteRecuperado>().currentWP;
                    //Guarda en la variable LastWP la posición a la que iba el agente en el momento de la detección
                    LastWP1 = Agente1.GetComponent<MovimientoAgenteRecuperado>().waypointsR[PathWP1].transform.position;
                }

                //Asigna la posicion actual del jugador al objetivo actual del agente
                switch (CantidadAgentes)
                {
                    case 5:
                        Agente1.GetComponent<MovimientoAgenteRecuperado>().waypointsR[PathWP1].transform.position = Player.transform.position;
                        Agente2.GetComponent<MovimientoAgenteRecuperado>().waypointsR[PathWP2].transform.position = Player.transform.position;
                        Agente3.GetComponent<MovimientoAgenteRecuperado>().waypointsR[PathWP3].transform.position = Player.transform.position;
                        Agente4.GetComponent<MovimientoAgenteRecuperado>().waypointsR[PathWP4].transform.position = Player.transform.position;
                        Agente5.GetComponent<MovimientoAgenteRecuperado>().waypointsR[PathWP5].transform.position = Player.transform.position;
                        break;
                    case 4:
                        Agente1.GetComponent<MovimientoAgenteRecuperado>().waypointsR[PathWP1].transform.position = Player.transform.position;
                        Agente2.GetComponent<MovimientoAgenteRecuperado>().waypointsR[PathWP2].transform.position = Player.transform.position;
                        Agente3.GetComponent<MovimientoAgenteRecuperado>().waypointsR[PathWP3].transform.position = Player.transform.position;
                        Agente4.GetComponent<MovimientoAgenteRecuperado>().waypointsR[PathWP4].transform.position = Player.transform.position;

                        break;
                    case 3:
                        Agente1.GetComponent<MovimientoAgenteRecuperado>().waypointsR[PathWP1].transform.position = Player.transform.position;
                        Agente2.GetComponent<MovimientoAgenteRecuperado>().waypointsR[PathWP2].transform.position = Player.transform.position;
                        Agente3.GetComponent<MovimientoAgenteRecuperado>().waypointsR[PathWP3].transform.position = Player.transform.position;
                        break;
                    case 2:
                        Agente1.GetComponent<MovimientoAgenteRecuperado>().waypointsR[PathWP1].transform.position = Player.transform.position;
                        Agente2.GetComponent<MovimientoAgenteRecuperado>().waypointsR[PathWP2].transform.position = Player.transform.position;
                        break;
                    case 1:
                        Agente1.GetComponent<MovimientoAgenteRecuperado>().waypointsR[PathWP1].transform.position = Player.transform.position;
                        break;
                    default:
                        print("WTF!!!");
                        break;
                }
                //Empieza una corrutina (para poder usar un temporizador)
                StartCoroutine(RecoverLastWP());
                //Cambia el valor de Unavez para no volver a entrar a este bucle
                UnaVez = 1;
                //Destroy(hit.collider.gameObject);	
            }

        }
    }


    //Corutina que se activa cuando el laser detecta el jugador.
    IEnumerator RecoverLastWP()
    {
        //Ponemos un tiempo de espera que será el tiempo que los agentes estén buscando al jugador
        yield return new WaitForSeconds(SearchTime);
        //Pasado este tiempo resetea la trayectoria original de los agentes
        switch (CantidadAgentes)
        {
            case 5:
                Agente1.GetComponent<MovimientoAgenteRecuperado>().waypointsR[PathWP1].transform.position = LastWP1;
                Agente2.GetComponent<MovimientoAgenteRecuperado>().waypointsR[PathWP2].transform.position = LastWP2;
                Agente3.GetComponent<MovimientoAgenteRecuperado>().waypointsR[PathWP3].transform.position = LastWP3;
                Agente4.GetComponent<MovimientoAgenteRecuperado>().waypointsR[PathWP4].transform.position = LastWP4;
                Agente5.GetComponent<MovimientoAgenteRecuperado>().waypointsR[PathWP5].transform.position = LastWP5;
                break;
            case 4:
                Agente1.GetComponent<MovimientoAgenteRecuperado>().waypointsR[PathWP1].transform.position = LastWP1;
                Agente2.GetComponent<MovimientoAgenteRecuperado>().waypointsR[PathWP2].transform.position = LastWP2;
                Agente3.GetComponent<MovimientoAgenteRecuperado>().waypointsR[PathWP3].transform.position = LastWP3;
                Agente4.GetComponent<MovimientoAgenteRecuperado>().waypointsR[PathWP4].transform.position = LastWP4;

                break;
            case 3:
                Agente1.GetComponent<MovimientoAgenteRecuperado>().waypointsR[PathWP1].transform.position = LastWP1;
                Agente2.GetComponent<MovimientoAgenteRecuperado>().waypointsR[PathWP2].transform.position = LastWP2;
                Agente3.GetComponent<MovimientoAgenteRecuperado>().waypointsR[PathWP3].transform.position = LastWP3;
                break;
            case 2:
                Agente1.GetComponent<MovimientoAgenteRecuperado>().waypointsR[PathWP1].transform.position = LastWP1;
                Agente2.GetComponent<MovimientoAgenteRecuperado>().waypointsR[PathWP2].transform.position = LastWP2;
                break;
            case 1:
                Agente1.GetComponent<MovimientoAgenteRecuperado>().waypointsR[PathWP1].transform.position = LastWP1;
                break;
            default:
                print("Incorrect intelligence level.");
                break;
        }


        //Y cambia el valor de UnaVez para que se pueda volver a detectar al jugador y asignar variables correctamente.
        UnaVez = 0;
        // esto no lo necesitamos ? --> currentAgent = 0;
    }
}
