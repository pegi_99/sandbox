﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour {

	public static AudioManager instance; 
	public AudioSource sound;
	void Update ()
	{
		if (PauseMenu.GameIsPaused) 
		{
			sound.volume = .2f;
		}
	}
}
