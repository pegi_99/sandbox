﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Puertacorrederavertical : MonoBehaviour {

    //La variable puerta se usará para conocer en todo momento si la puerta está cerrada (0) o abierta (1);
    int puerta = 0;
    float apertura = 2.28f;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Agente")
        {
            if (puerta == 0)
            {
                transform.Translate(Vector3.up * apertura);
                puerta = 1;
            }
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Agente")
        {
            StartCoroutine(AgentIsGone());

        }
    }

    void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player")
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                if (puerta == 0)
                {
                    transform.Translate(Vector3.up * apertura);
                    puerta = 1;
                }
                else
                {
                    transform.Translate(-Vector3.up * apertura);
                    puerta = 0;
                }
            }
        }

    }

    IEnumerator AgentIsGone()
    {
        //Ponemos un tiempo de espera que se usará para saber que el agente ha pasado por la puerta
        yield return new WaitForSeconds(2f);
        if (puerta == 1)
        {
            transform.Translate(-Vector3.up * apertura);
            puerta = 0;
        }
    }
}