﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


//Script donde guardamos las variables del jugador.
//Aquí deberemos almacenar también los datos de objetos conseguidos.

public class PlayerState : MonoBehaviour {
	public static PlayerState Instance;
	public Transform playerPosition;
	//Obtener variables de Serializables.cs : 
	public PlayerStatistics localPlayerData = new PlayerStatistics();

	void Awake ()
	{
		if (Instance == null)
			Instance = this;
		if (Instance != this)
			Destroy (gameObject);

		//GlobalControl.Instance.Player = gameObject;
	}

	void Start ()
	{
		localPlayerData = GlobalControl.Instance.savedPlayerData;
	}
}
