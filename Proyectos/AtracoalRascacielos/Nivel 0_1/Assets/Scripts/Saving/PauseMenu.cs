﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour {

	//Variable que nos dirá si el juego está o no en pausa
	public static bool GameIsPaused = false;
	public GameObject pauseMenuUI;
	
	// Update is called once per frame
	void Update () 
	{
		//Se presiona la tecla Esc para ir al menú o salir de él
		if (Input.GetKeyDown (KeyCode.Escape)) 
		{
			//Si estamos en pausa
			if (GameIsPaused) 
			{
				Resume ();
			} else 
			{
				Pause();
			}
		}
	}

	//Método para retomar el juego, lo hacemos público para poder utilizarlo en el botón de Resume también.
	public void Resume()
	{
		//Desactivar el menú:
		pauseMenuUI.SetActive (false);
		//Reactivar el tiempo del juego:
		Time.timeScale = 1f;
		//Cambiar el valor de GameIsPaused:
		GameIsPaused = false;

	}

	//Método para pausar el juego:
	void Pause()
	{
		//Activar el menú:
		pauseMenuUI.SetActive (true);
		//Parar el tiempo del juego:
		Time.timeScale = 0f;
		//Cambiar el valor de GameIsPaused:
		GameIsPaused = true;
	}

	//Aquí irá el script para provocar el guardado
	public void SaveGame()
	{
		//PlayerState.Instance.localPlayerData.SceneID = Application.loadedLevel;
		PlayerState.Instance.localPlayerData.SceneID = SceneManager.GetActiveScene().buildIndex;
		PlayerState.Instance.localPlayerData.PositionX = transform.position.x;
		PlayerState.Instance.localPlayerData.PositionY = transform.position.y;
		PlayerState.Instance.localPlayerData.PositionZ = transform.position.z;

		GlobalControl.Instance.SaveData();
		//Ponemos este mensaje para asegurarnos de que el botón corre el contenido
		Debug.Log("Saving game...");
	}

	//Volver al menú principal, cuya escena es la 0 según el build:
	public void MainMenu()
	{
		SceneManager.LoadScene (0);
	}

	//salir del juego directamente:

	public void QuitGame()
	{
		//Función para salir:
		Application.Quit();
		//No se activará mientras estemos trabajando en Unity, utilizamos este mensaje para asegurarnos de que el contenido corre al pulsar el botón:
		Debug.Log ("Quitting game..");
	}
}
