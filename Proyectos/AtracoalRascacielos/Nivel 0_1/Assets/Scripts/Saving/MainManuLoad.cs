﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class MainManuLoad : MonoBehaviour {

	public PlayerStatistics LocalCopyOfData;

	public void Load ()
	{
		BinaryFormatter formatter = new BinaryFormatter ();
		//Abrir el archivo:
		FileStream saveFile = File.Open ("Saves/AtracoRascacielos.pegi99", FileMode.Open);
		//Copiar los datos en el archivo guardado en la variable LocalCopyOfData
		//Todavía no lo cargamos a PlayerState, necesitamos saber en qué escena estamos:
		LocalCopyOfData = (PlayerStatistics)formatter.Deserialize (saveFile);

		SceneManager.LoadScene (LocalCopyOfData.SceneID);
		saveFile.Close ();
	
	}
}
