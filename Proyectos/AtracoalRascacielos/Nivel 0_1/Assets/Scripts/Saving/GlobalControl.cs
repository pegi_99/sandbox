﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Runtime.Serialization.Formatters.Binary; //Para el Binary Formatter
using System.IO;


//Clase con patrón de diseño Singleton:
public class GlobalControl : MonoBehaviour {

	public static GlobalControl Instance;
	public PlayerStatistics savedPlayerData = new PlayerStatistics();
	public GameObject Player;

	//Método que crea una instancia del objeto solo si todavía no existe alguna:
	void Awake ()
	{
		//si no hay instancia:
		if (Instance == null) {
			//Esta parte se asegura de que el objeto al que está unido el script (GlobalObject) no es destruido en el cambio de escena.
			DontDestroyOnLoad (gameObject);
			//La instancia de la clase es esta.
			Instance = this;
		}
		else if (Instance != this)
		{
			//Si hay otra instancia de la clase se destruye.
			Destroy (gameObject);
		}
	}

	//Funciones de serialización:
	public PlayerStatistics LocalCopyOfData;
	public bool IsSceneBeingLoaded = false;

	public void SaveData()
	{
		//Patch check:
		//File.Create creará un nuevo archivo, pero no creará la ruta del directorio donde se debe guardar.
		//Se crea el directorio "Saves si este no existe":
		if (!Directory.Exists ("Saves"))
			Directory.CreateDirectory ("Saves");

		//Serializar los datos:

		BinaryFormatter formatter = new BinaryFormatter ();
		FileStream saveFile = File.Create ("Saves/AtracoRascacielos.pegi99"); //Crear archivo
		//Se puede utilizar cualquier extensión para el archivo, así que usamos una personalizada

		//Los datos son:
		LocalCopyOfData = PlayerState.Instance.localPlayerData;
		//Aquí realmente se serializan finalmente:
		formatter.Serialize (saveFile, LocalCopyOfData);
		saveFile.Close (); //Cerramos el archivo
	}

	public void LoadData()
	{

		BinaryFormatter formatter = new BinaryFormatter ();
		//Abrir el archivo:
		FileStream saveFile = File.Open ("Saves/AtracoRascacielos.pegi99", FileMode.Open);
		//Copiar los datos en el archivo guardado en la variable LocalCopyOfData
		//Todavía no lo cargamos a PlayerState, necesitamos saber en qué escena estamos:
		LocalCopyOfData = (PlayerStatistics)formatter.Deserialize (saveFile);

		saveFile.Close ();
	}

}
