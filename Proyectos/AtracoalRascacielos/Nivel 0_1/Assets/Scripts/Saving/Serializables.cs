﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable] //Comunica a UnityEngine que los datos en esta clase son adecuados para ser serializados
public class PlayerStatistics
{
	public int SceneID; //Variable para identificación de escena
	public float PositionX, PositionY, PositionZ; //Variables para posición del jugador

	public float HP; //Salud
	public float XP; //Experiencia
};
