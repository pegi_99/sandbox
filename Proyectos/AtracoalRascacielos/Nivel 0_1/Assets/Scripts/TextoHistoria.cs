﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextoHistoria : MonoBehaviour {

    public GameObject textO;
	
	public GameObject historia;
    

    void Start()
    {
        textO.SetActive(false);
        
    }
 void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            textO.SetActive(true);
            
        }
    }
    void OnTriggerExit(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            textO.SetActive(false);
			historia.SetActive(false);
           
        }
    }
	
    void OnTriggerStay(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
               textO.SetActive(false);
			   historia.SetActive(true);
			   
                
            }
        }
    }
}
