﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AbrePuerta : MonoBehaviour {

	// La variable i es una variable booleana que almacena el estado de apertura ( abierto o cerrado ) de la puerta
	int i = 1;
	//Quaternion se utiliza para guardar la rotación deseada de la puerta
	Quaternion targetRotation;
	//Los vectores Abierta y Cerrada son variables que se usaran para definir las direcciones que tendrá el canto de la puerta cuando
	//esté abierta o cerrada y estas direcciones se usarán para definir los quaternios que permitirán abrir o cerrar la puerta lentamente
	//mediante la funcion Quaternion.Lerp
	Vector3 Abierta;
	Vector3 Cerrada;
	// Esta variable se utiliza para para no estar calculando constantemente en la funcion Update ya uqe sólo queremos que se actualice
	//la puerta cuando presionemos la E
	bool rotating = false;
	//RotationTime almacena la velocidad a la que gira la puerta y solo puede valer entre 0 y 1 porque la funcion Quaternion.lerp funciona así
	float RotationTime;
	//Speed es una variable pública que se podra usar para cambiar la velocidad de la puerta
	public float speed;

	//Al inicializar la puerta definimos 2 direcciones respecto a su posicion inicial,esto permite girar una cantidad fija cualquier puerta
	//independientemente de su orientación inicial
	void Start () 
	{
		Abierta = transform.forward;
		Cerrada = transform.right;
	}

	//Update se llama en cada frame, y en este caso se activará su codigo al presionar la tecla E delante de una puerta
	void Update()
	{
		//Al presionar la tecla E cambiaremos el estado de la variable rotating y se activará este codigo
		if (rotating) 
		{
			//Utilizamos la funcion Quaternion.Lerp para definir en todo instante la rotación de la puerta y  que permitira girarla 
			//lentamente desde su orientacion actual hasta la orientacion objetivo que se almacena en la variable targetRotation
			RotationTime += Time.deltaTime * speed;
			transform.rotation = Quaternion.Lerp (transform.rotation, targetRotation, RotationTime);
			//Cuando la puerta ha terminado de girar (rotationtime = 1) cambiamos el valor de la variable rotating para que en el siguiente
			//ciclo Update no tengamos que calcular la rotacion actual de la puerta
			if (RotationTime > 1) 
			{
				rotating = false;
			}
		}
	}
		
	//El GameObject Puerta tiene un collider . Si algo entra en el collider llamamos a la siguiente funcion
	void OnTriggerStay(Collider other)
    {
		//Si lo que ha entrado en el collider es el jugador

        if (other.tag=="Player")
		{
			// Y además presionamos la E
            if (Input.GetKeyDown(KeyCode.E))
			{	
				//Si la puerta esta cerrada
				if (i == 1) 
				{
					//La rotacion objetico es la de la puerta abierta
					targetRotation = Quaternion.LookRotation (Abierta);
					//Y cambia el estado de apertura de la puerta
					i = 0;
				}
				//Si la puerta estaba abierta , haz lo contrario a lo anterior.
				else 
				{
					targetRotation = Quaternion.LookRotation (Cerrada);
					i = 1;
				}
				//Cambiamos el valor de la variable rotating para que en el siguiente ciclo update se actue sobre la rotacion de la puerta
				rotating = true;
				//Le decimos a la función Quaternion.Lerp que la puerta no ha empezado a rotar
				RotationTime = 0;
			}
		}
	}
}