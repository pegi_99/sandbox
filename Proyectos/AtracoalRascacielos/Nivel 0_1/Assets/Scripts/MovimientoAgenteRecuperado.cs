﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

//Este codigo permite ser aplicado a cualquier GameObject Agente y definir unos puntos principales de su camino para que el agente
//se mueva ciclicamente entre ellos. Para ello se debe generar una NavigationMesh en el plano principal de la escena y aplicar un
//NavMeshAgent en el inspector del agente.

public class MovimientoAgenteRecuperado : MonoBehaviour { 
	
	
	
	//Vector de GameObjects que permite definir el numero de variables del vector por el inspector. En cada variable del vector se almacena
	//la posicion del gameobject al que hace referencia. Cada uno de estos objetos sera un punto en el camino del agente y no deberan ser vistos en la escena principal.
	public GameObject[] waypointsR;
	//variable para contar el punto del camino del agente al que el agente esta yendo actualmente.
    public int currentWP = 0;
	//Aqui se definen las variables para el movimiento del agente pero creo que no son necesarias ya que te dejan modificarlo por la malla de navegacion
	
	public float accuracyWP =5.0f; 
	
	//variable para guardar una referencia a la malla de navegacion
	NavMeshAgent _navMeshAgent;
	
	// Al iniciar el script se guarda una referencia a la malla de navegacion en la variable _navMeshAgent. En caso de que no exista una malla de
	//navegacion en la escena, no hacer nada. En caso de que si exista, llama a la funcion SetDestination().
	void Start () 
	{
		_navMeshAgent = this.GetComponent<NavMeshAgent>();
		if (_navMeshAgent == null)
		{

		} 
		else 
		{ 
			SetDestination();
		}
	}

	//En cada pantallazo se llama a la funcion update. En este caso si la distancia del agente a su siguiente punto en el camino es menor que
	//una distancia definida por accuracyWP, entonces se cambia en siguiente punto del camino.
	void Update() 
	{
		if(Vector3.Distance(waypointsR[currentWP].transform.position, transform.position)<accuracyWP)
		{
			currentWP++;
			if(currentWP>=waypointsR.Length)
			{
				currentWP=0;
			}
		}
		SetDestination();
	}

	//funcion que le pasa a la malla de navegacion la posicion objetivo a la que tiene que moverse el agente. Se llama a la funcion navmesh.setdestination
	//qeu esta definida en unity y ella solita te calcula el camino.
	private void SetDestination()
	{ 
		if(waypointsR[currentWP] != null){
			Vector3 targetVector = waypointsR[currentWP].transform.position;
			_navMeshAgent.SetDestination(targetVector);}

	}}