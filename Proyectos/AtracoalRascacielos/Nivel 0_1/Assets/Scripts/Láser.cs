﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Láser : MonoBehaviour {

	// Use this for initialization
	// Declaramos una variable pública para guardar el material de la luz del láser(material rojo en este caso)
	public Material Material;
	void Start () 
	{
		//Declaramos una variable donde almacenar el componente LineRenderer (debe haber un componente LineRenderer en el objeto de este código. Creo que 
		//para poner este componente Unity>Componentes>Efectos>LineRender)
		LineRenderer lineRenderer = GetComponent<LineRenderer>();
		//Asignamos el material creado anteriormente al material del láser
		 lineRenderer.material =  Material;
	}
	
	// Update is called once per frame
	void Update () 
	{
		//Hay que volver a asignar la variable lineRenderer al componente LineRenderer. El lineRender permite dibujar lineas en Unity.
		LineRenderer lineRenderer = GetComponent<LineRenderer>();
		//Definimos el punto de partida del laser, que sera igual a la posicion del láser.Como este código va en el laser transform.position
		lineRenderer.SetPosition(0, transform.position);
		//Lanzamos un Raycast desde el laser hacia delante(en su propio sistema de coordenadas) y almacenamos los resultados en la variable hit.
		RaycastHit hit;	
		Physics.Raycast(transform.position,transform.forward, out hit);

        //Destruye al personaje(ejemplo de acción cuando el laser te pilla). [El objetivo es que los guardias corran mas rápido y vayan a por ti
        //durante un tiempo determinado]-------------->	
        //if (hit.collider.tag == "Player"){  Destroy(hit.collider.gameObject);}
		
		//Define el punto de llegada del láser, igual al primer punto de choque del raycast. Esto dibuja una linea desde el laser hasta ese punto. 
		lineRenderer.SetPosition(1, hit.point);
	}
}
