﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangingLevel : MonoBehaviour 
{
	//private string scene = "Level0";

	void OnTriggerEnter(Collider other)
	{
		if (other.CompareTag ("Player")) {
			Initiate.Fade (Color.black, 0.5f); 	//Esta línea es para activar dos scripts y que haya fade in entre escenas, pero de momento no he conseguido que funcione. Los scripts a los que "llama" son: Initiate y Fader.
			SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex + 1);
		}
	}

}
