﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;


public class Menu_Play : MonoBehaviour {
	public void PlayGame()
	{
		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
	}
	public PlayerStatistics LocalCopyOfData;

	public void Load ()
	{
		if (System.IO.File.Exists ("Saves/AtracoRascacielos.pegi99")) {
			BinaryFormatter formatter = new BinaryFormatter ();
			//Abrir el archivo:
			FileStream saveFile = File.Open ("Saves/AtracoRascacielos.pegi99", FileMode.Open);
			//Copiar los datos en el archivo guardado en la variable LocalCopyOfData
			//Todavía no lo cargamos a PlayerState, necesitamos saber en qué escena estamos:
			LocalCopyOfData = (PlayerStatistics)formatter.Deserialize (saveFile);

			SceneManager.LoadScene (LocalCopyOfData.SceneID);
			saveFile.Close ();
		} else {
			SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
		}


	}
}
