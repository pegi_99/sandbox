﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//El siguiente código consigue volver transparente cualquier objeto que entre entre la camara y el jugador para tener siempre visón sobre el mismo.
//El codigo se debe aplicar a un objeto hijo del Player que se encuentre en la misma posicion y sin ninguna rotación aplicada. Ese Objeto hijo debe contener
//un  capsule collider situado entre el Player y la camara principal. El collider tendra un radio de 0.3 y una altura igual a la distancia entre camara y jugador.
//Ademas se debe crear un nuevo material transparente y configurarlo al gusto. Este material se aplicara a los objetos entre la camara y el jugador. En la referencia
//a material que aparece en el inspector se debe colocar este nuevo material.

public class transparencias : MonoBehaviour
{
    //Creamos un espacio en el inspector de Unity para colocar referencia la material que se aplicara al objetco entre la cámara y la pared. Este material
    //debe ser configurado para ser transparente( Usar un shader transparent)
    public Material Transparente;
    //añadimos una referencia en el inspector para el Player
    public GameObject Cube;
    //Creamos una variable para almacenar el material del objeto que ha entrado en el campo de visión
    new Material mat;


    //Cuando el Objeto entra en el Collider se lee el material del objeto que ha colisionado y se almacena en la variable mat
    private void OnTriggerEnter(Collider other)
    {
        mat = other.GetComponent<Renderer>().material;
    }

    //Mientras el material se encuentra en el campo de visión se le aplica el material transparente
    private void OnTriggerStay(Collider other)
    {
                    if (other.tag != "Suelo" && other.tag != "Player")
                    {
                            
                            other.GetComponent<Renderer>().material = Transparente;
                    }
    }

    //Al salir del campo de vision se debe volver a aplicar el material original al objeto que entro en el campo de vision para que no permanezca transparente
    private void OnTriggerExit(Collider other)
    {
        // if (other.tag != "Suelo" && other.tag != "Player")
        // {

        other.GetComponent<Renderer>().material = mat;
        // }
    }


}

