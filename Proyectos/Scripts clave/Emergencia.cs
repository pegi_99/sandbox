﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Emergencia : MonoBehaviour {


    private Light luz;
    public float duration = 1.0f;
    private float max;
    // Use this for initialization
    void Start ()
    {
        luz = GetComponent<Light>();
        max = luz.intensity;
        luz.color = Color.red;
	}
	
	// Update is called once per frame
	void Update ()
    {
        float phi = Time.time / duration * 2 * Mathf.PI;
        float amplitude = Mathf.Abs(max * Mathf.Cos(phi) * 0.5f);
        luz.intensity = amplitude;

    }
}
