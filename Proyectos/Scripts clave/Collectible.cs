﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectible : MonoBehaviour {
    //Variable publica para el texto cuenta de coleccionables:
//    public Text countText;
    //Variable privada para guardar número de coleccionables recogidos
    private int count;
	// Al inicio del juego
	void Start () {
        
        //Se inicia count en 0
        count = 0;
//        SetCountText ();

	}

    // Cuando el jugador se cruza con un collider con 'is trigger' activado, guarda una referencia a este collider en la variable "other"
	void OnTriggerEnter (Collider other) {
        //Si el game object con el que se cruza tiene la etiqueta 'Collectible' asignada:
        if (other.gameObject.CompareTag("Collectible"))
        {
            // Hacer el coleccionable invisible
            other.gameObject.SetActive(false);

            // Añadir una unidad a la cuenta de coleccionables recogidos
            count = count + 1;

            // Run la función SetCountText (que esta debajo)
  //          SetCountText();
        }
	}

    // Funcion SetCountText
//    void SetCountText()
 //   {
        // Actualizar el texto de cuenta de coleccionables:
 //       countText.text = "Coleccionables: " + count.ToString();
 //   }
}
