﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeHeVisto : MonoBehaviour {
	//En este codigo se presenta una solucion para el cono de visión de los agentes. Este código 
	//se debe aplicar a un cono hijo del gameobject Cube y que represente su campo de visiony que ademas debe contener un collider que hará
	//los triggers de este codigo. Cube en este código guarda una referencia al agente al que esta aplicado el campo de vision que tiene este 
	//codigo. Player hace referencia al Jugador principal.
	
	//referencia al Jugador principal
    public GameObject player;
	//referecnia al agente
    public GameObject Cube;
	//variable para guardar el resultado del raycast
    RaycastHit Rayo;
	
	//Mientras otro objeto se encuentre dentro del collider, si es el objeto Jugador, lanza un rayo entre el agente que tiene este codigo y el jugador.
	//si el raycast ha chocado con otro objeto antes del jugador, entonces el agente no tiene vision osbre el mismo. En caso contrario, para este ejemplo,
	//el agente tiene vision sobre el jugador y el jugador es destruido.
    private void OnTriggerStay(Collider other)
    {
        if(other.tag == "Player")
        {

            Vector3 dir = player.transform.position-Cube.transform.position;
            if (Physics.Raycast(Cube.transform.position, dir ,out Rayo))
            {
                //print("El rayo ha impactado contra algo");
                if(Rayo.collider != null)
                {
                    //print("Rayo.collider != null");
                    if (Rayo.collider.tag == "Player") 
					{
						//Accion a realizar cuando el agente ve al jugador. En este caso se destruye el objeto principal.
						Destroy(other.gameObject); 
					}
				}
            }

        }
    }
}